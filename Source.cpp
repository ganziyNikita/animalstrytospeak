#include <iostream>
#include <clocale>
#include "animals/Animals.h"

int main()
{
	Animals *cow = new Cow();
	Animals *cat = new Cat();
	Animals *dog = new Dog();

	cow->say();
	cat->say();
	dog->say();

	system("pause");
	return 0;
}